class Player < ActiveRecord::Base

  default_scope { order(score: :desc) }

  validates_presence_of :name

  def incress_score
    self.score += 1
    save
  end

  def decrease_score
    self.score -= 1
    save
  end

end
